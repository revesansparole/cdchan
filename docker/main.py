"""
This is the entry point for docker.
"""
import contextlib
import socket  # For gethostbyaddr()
from functools import partial
from http.server import SimpleHTTPRequestHandler, ThreadingHTTPServer, test as launchserver
from subprocess import run

# perform indexing of condarepo
cmd = ["conda", "index", "/condarepo/"]

run(cmd)

# launch file server (copy from http.server)
handler_class = partial(SimpleHTTPRequestHandler, directory="condarepo")


# ensure dual-stack is not disabled; ref #38907
class DualStackServer(ThreadingHTTPServer):
    def server_bind(self):
        # suppress exception when protocol is IPv4
        with contextlib.suppress(Exception):
            self.socket.setsockopt(
                socket.IPPROTO_IPV6, socket.IPV6_V6ONLY, 0)
        return super().server_bind()


launchserver(
    HandlerClass=handler_class,
    ServerClass=DualStackServer,
    port=8910,
    bind=None,
)
